# GSStingray

[![](https://jitpack.io/v/com.gitlab.GSLabsAndroidProjects/GSStingray.svg)](https://jitpack.io/#com.gitlab.GSLabsAndroidProjects/GSStingray)

STB configuration SDK for Android.

- [Installation](#installation)
- [API](#api)
    - [Client](#client)
    - [Browser](#browser)
- [Examples](#examples)
    - [Turn the power on](#turn-the-power-on)
    - [Get channel list](#get-channel-list)
    - [Switch the channel](#switch-the-channel)

## Installation

build.gradle (Project)

```gradle
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```

build.gradle (Module)

```gradle
dependencies {
    implementation 'com.gitlab.GSLabsAndroidProjects:GSStingray:1.4'
}
```

## API

The API consists of 2 main interfaces - Client and Browser.

### Client

`StgClient` class provides high level API for making HTTP requests to the STB - PowerSet, ChannelsGet, ChannelSet, etc. Each method has its synchronous and asynchronous version. Synchronous version ends with `Sync` suffix and blocks the current thread until request is completed. Asynchronous version ends with `Async` suffix and invokes a callback when request is completed. All callbacks are posted to the application's main thread.

```java
StgClient client = StgClient.client("http://192.168.0.1:50000", "stingray");
```

### Browser

`StgBrowser` class discovers the STBs in local network and resolves their IP addresses. Resolved addresses can be used by `StgClient` instances for making HTTP requests to the STBs.

```java
browser = StgBrowser.browser(this, "_stingray-remote._tcp");
browser.listeners.add(this);
browser.start();

public void endpointFound(StgBrowser sender, StgEndpoint endpoint) {
    StgClient client = StgClient.client(endpoint, "stingray");
}
```

## Examples

Common use cases are described below.

### Turn the power on

Before making requests, ensure that STB is out of standby mode by turning it explicitly on.

```java
client.powerSetAsync(true, (exception) -> {
    if (exception == null) {
        // Success
    } else {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
});
```

### Get channel list

Obtain the list of available channels.

```java
client.channelsGetAsync((channels, exception) -> {
    if (channels == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Success
    }
});
```

### Switch the channel

Make the channel active.

```java
StgChannel channel = new StgChannel();
channel.channelListId = "TV";
channel.channelNumber = 2;

client.channelSetAsync(channel, (exception) -> {
    if (exception == null) {
        // Success
    } else {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
});
```
